package com.mycompany.msbconsulta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.mycompany.msbconsulta.interfaces")
@EnableEurekaClient
public class MsbConsultaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsbConsultaApplication.class, args);
	}

}
