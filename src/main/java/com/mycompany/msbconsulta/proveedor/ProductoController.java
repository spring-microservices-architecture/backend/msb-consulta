package com.mycompany.msbconsulta.proveedor;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.msbconsulta.dto.ProductoDto;
import com.mycompany.msbconsulta.dto.ProductoReadDto;
import com.mycompany.msbconsulta.dto.ResponseDto;
import com.mycompany.msbconsulta.interfaces.ProductoCbInterface;
import com.mycompany.msbconsulta.interfaces.ProductoInterface;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private ProductoInterface productoInterface;

	@Autowired
	private ProductoCbInterface productoCbInterface;

	@CircuitBreaker(name = "productCB", fallbackMethod = "fallBackGetAllProduct")
	@GetMapping
	public ResponseEntity<List<ProductoReadDto>> getAll() {
		return ResponseEntity.ok().body(productoInterface.getConsulta());
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<ProductoReadDto> getConsultaByCodigo(@PathVariable(value = "codigo") Long codigo) {
		return ResponseEntity.ok().body(productoInterface.getConsultaByCodigo(codigo));
	}

	@GetMapping("/descripcion/{descripcion}")
	public ResponseEntity<List<ProductoReadDto>> getConsultaByDescripcion(
			@PathVariable(value = "descripcion", required = true) String descripcion) {
		return ResponseEntity.ok().body(productoInterface.getConsultaByDescripcion(descripcion));
	}

	@PostMapping
	public ResponseEntity<ResponseDto> createConsulta(@Valid @RequestBody ProductoDto dataDto) throws Exception {
		return new ResponseEntity<>(productoInterface.saveConsulta(dataDto), HttpStatus.CREATED);
	}

	@PutMapping("/{codigo}")
	public ResponseEntity<ResponseDto> updateConsulta(@PathVariable(value = "codigo") Long codigo,
			@Valid @RequestBody ProductoDto dataDto) throws Exception {
		return new ResponseEntity<>(productoInterface.updateConsulta(codigo, dataDto), HttpStatus.OK);
	}

	@DeleteMapping("/{codigo}")
	public ResponseEntity<ResponseDto> deleteConsulta(@PathVariable(value = "codigo") Long codigo) {
		return ResponseEntity.ok().body(productoInterface.deleteConsulta(codigo));
	}

	private ResponseEntity<List<ProductoReadDto>> fallBackGetAllProduct(RuntimeException ex) {
		return ResponseEntity.ok().body(productoCbInterface.getConsulta());
	}
}