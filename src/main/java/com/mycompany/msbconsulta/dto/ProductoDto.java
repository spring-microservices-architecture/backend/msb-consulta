package com.mycompany.msbconsulta.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ProductoDto {

	@NotNull(message = "El campo descripcion no puede ser nulo.")
	@Size(min = 2, max = 50, message = "El campo 'descripcion' debe tener entre 2 y 50 caracteres")
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private String descripcion;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer precio;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
}