package com.mycompany.msbconsulta.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ResponseDto {
	private Date timestamp = new Date();

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String message;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Object obj;

	public ResponseDto() {}

	public ResponseDto(String message) {
		super();
		this.message = message;
	}

	public ResponseDto(String message, Object obj) {
		super();
		this.message = message;
		this.obj = obj;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}
}