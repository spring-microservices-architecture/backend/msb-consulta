package com.mycompany.msbconsulta.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ProductoReadDto {

	@JsonInclude(JsonInclude.Include.NON_NULL)
    private Long codigo;
	
	@NotNull(message = "El campo descripcion no puede ser nulo.")
	@Size(min = 2, max = 50, message = "El campo 'descripcion' debe tener entre 2 y 50 caracteres")
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private String descripcion;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer precio;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
}