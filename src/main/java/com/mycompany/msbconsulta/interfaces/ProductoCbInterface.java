package com.mycompany.msbconsulta.interfaces;

import java.util.List;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.mycompany.msbconsulta.dto.ProductoReadDto;

@FeignClient(name = "ms-cb-consulta", contextId = "ctxCbConsulta", path = "mscbconsulta")
@LoadBalancerClient(name = "ms-cb-consulta")
public interface ProductoCbInterface {

	@GetMapping(path = "/productos")
	public List<ProductoReadDto> getConsulta();

	@GetMapping(path = "/productos/{codigo}")
	public ProductoReadDto getConsultaByCodigo(@PathVariable(value = "codigo") Long codigo);
	
	@GetMapping(path = "/productos/descripcion/{descripcion}")
	public List<ProductoReadDto> getConsultaByDescripcion(@PathVariable(value = "descripcion") String descripcion);
}