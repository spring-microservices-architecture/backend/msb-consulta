package com.mycompany.msbconsulta.interfaces;

import java.util.List;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mycompany.msbconsulta.dto.ProductoDto;
import com.mycompany.msbconsulta.dto.ProductoReadDto;
import com.mycompany.msbconsulta.dto.ResponseDto;

@FeignClient(name = "ms-consulta", contextId = "ctxConsulta", path = "msconsulta")
@LoadBalancerClient(name = "ms-consulta")
public interface ProductoInterface {

	@GetMapping(path = "/productos")
	public List<ProductoReadDto> getConsulta();

	@GetMapping(path = "/productos/{codigo}")
	public ProductoReadDto getConsultaByCodigo(@PathVariable(value = "codigo") Long codigo);

	@GetMapping(path = "/productos/descripcion/{descripcion}")
	public List<ProductoReadDto> getConsultaByDescripcion(@PathVariable(value = "descripcion") String descripcion);

	@PostMapping("/productos")
	public ResponseDto saveConsulta(@RequestBody ProductoDto dataDto);

	@PutMapping("/productos/{codigo}")
	public ResponseDto updateConsulta(@PathVariable(value = "codigo") Long codigo, @RequestBody ProductoDto dataDto);

	@DeleteMapping("/productos/{codigo}")
	public ResponseDto deleteConsulta(@PathVariable(value = "codigo") Long codigo);
}